package com.api.rest.rpg.characterlastnames;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharacterlastnamesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CharacterlastnamesApplication.class, args);
	}

}
