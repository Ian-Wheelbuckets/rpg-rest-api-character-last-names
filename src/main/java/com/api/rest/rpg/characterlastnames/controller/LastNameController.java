package com.api.rest.rpg.characterlastnames.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.rest.rpg.characterlastnames.dao.LastNameRepository;
import com.api.rest.rpg.characterlastnames.exception.UnreachableLastNamesException;
import com.api.rest.rpg.characterlastnames.model.LastName;

//import com.api.rest.rpg.characterfirstnames.dao.CountryRepository;
//import com.worldfirstnames.model.Country;

@RestController
@RequestMapping("/api")
public class LastNameController {
	
	@Autowired
	private LastNameRepository lastNameRep;
	
	@RequestMapping(value="/last-names", method=RequestMethod.GET)
	public List<LastName> getAllLastNames() {
		List<LastName> lns = lastNameRep.findAll();
		
		if(lns==null)
			throw new UnreachableLastNamesException("The serveur is unable to reach the list of last names.");
		
		return lns;
	}
	
	@RequestMapping(value="/last-names/id/{id}", method=RequestMethod.GET)
	public Optional<LastName> getLastNameById(@PathVariable Integer id) {
		Optional<LastName> ln = lastNameRep.findById(id);
		
		if(!ln.isPresent())
			throw new UnreachableLastNamesException("The serveur is unable to reach the  last name.");
		
		return ln;
	}
}
