package com.api.rest.rpg.characterlastnames.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.rest.rpg.characterlastnames.model.LastName;

@Repository
public interface LastNameRepository extends JpaRepository<LastName, Integer> {

}
