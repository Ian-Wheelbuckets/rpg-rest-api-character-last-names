package com.api.rest.rpg.characterlastnames.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UnreachableLastNamesException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnreachableLastNamesException(String s) {
		super(s);
	}
	
}
